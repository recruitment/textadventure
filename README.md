# The quest for epic loot

## overview 

A simple text adventure, code based on https://github.com/WebDevSimplified/JavaScript-Text-Adventure

This game is intended to be played at job fairs to enable some interaction with applicants. To win the game, players must either interact with the SWITCH website or staff at the booth to find the answers to the questions in the game or alternatively they can "hack" the game by opening the javascript console and mess with the game state.

## Player Goal

The goal of the player is to retrieve the secret code required to take part in the raffle to win a price. 

## Booth operator's goals

 * Motivate people to visit the booth ( "Do you wanna play a game?" / "Win a price?" )
   * tell them that **EVERYTHING** is allowed to get the code word. 
 * Interact with potential candidates
 * Find out what interests them by watching them play the game. For example, people opening the javascript console to "cheat" the game might be candidates for Security, Web development, ... openings
 * If players are stuck in the game, help them find the solution by telling them about Switch / give them hints where stuff is on our website


## Setup

The game is just static html + javascript, so it can be played by putting the files on any webserver. To play it locally on your computer you can run (from the project root directory): 

```
python3 -m http.server --directory src 1212
```

This will start a local webserver and serve the game from the `src` folder under the URL:

http://localhost:1212/


## Maintenance 

* To change the game flow, edit the `textNodes` blocks
* To change the code word, encode it in BASE64 and update the `SECRET_CODE` variable

