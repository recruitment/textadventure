/**
 * Ploetzlich wird dir alles klar! Es ist nur eine Simulation! Aus voller Kehle schreist du "F12"! 
 * Die Waende der Messehalle brechen zusammen und dahinter siehst du ein Gewirr aus scheinbar voellig
 * zusammenhangslosen Woertern und Zeichen. Ob du damit wohl etwas anfangen kannst?
 * 
 * 
 */

const textElement = document.getElementById('text')
const optionButtonsElement = document.getElementById('option-buttons')
const startButton = document.getElementById('btnStart')


let state = {
}

function startGame() {
  state = {
    lock1_open: false,
    lock2_open: false,
    lock3_open: false,
  }

  console.clear()
  console.warn("Völlig unbeeindruckt von diesem Hokuspokus drückst du die nächstgelegene F12-Taste.")
  console.warn("Vor dir am Boden öffnet sich eine Luke. Darunter siehst du über einen Tisch gebeugt INSPECTOR F. ZWÖLF, welcher sich gerade hämisch grinsend diverse Javascript-Variablen anschaut.")
  console.warn("Uh-oh.. er hat dich anscheinend bemerkt und schaut zu dir hoch.")
  console.error("HEY! WAS TUST DU HIER! DIESER BEREICH ist.äh.. verboten! Bist du etwa ein:e HACKER:IN ???")
  console.warn("Schnell wirft er ein Tuch über den Tisch mit den verschiedenen Variablen. Du kannst gerade noch einen Blick auf einen Variablennamen erhaschen: 'state' ")
  console.log(state)
  showTextNode(1)
}

startButton.addEventListener('click', () => window.location.reload(true))

function showTextNode(textNodeIndex) {
  const textNode = textNodes.find(textNode => textNode.id === textNodeIndex)
  if(typeof textNode.text === 'function'){
    textElement.innerText = textNode.text(state)
  } else {
    textElement.innerText = textNode.text
  }
  
  while (optionButtonsElement.firstChild) {
    optionButtonsElement.removeChild(optionButtonsElement.firstChild)
  }

  textNode.options.forEach(option => {
    if (showOption(option)) {
      const button = document.createElement('button')
      button.innerText = option.text
      
      button.classList.add('btn')
      button.addEventListener('click', () => selectOption(option))
      optionButtonsElement.appendChild(button)
    }
  })
}

function showOption(option) {
  return option.requiredState == null || option.requiredState(state)
}

const SECRET_CODE='U1dJVENISUU='

function selectOption(option) {
  const nextTextNodeId = option.nextText
  if (nextTextNodeId <= 0) {
    return startGame()
  }
  state = Object.assign(state, option.setState)
  showTextNode(nextTextNodeId)
}

function boolToDoorLockState(b){
  return b?'offen':'verschlossen'
}

function inspectorComments(currentState){
  let num_locks = 0
  if(currentState.lock1_open){
    num_locks+=1
  }
  if(currentState.lock2_open){
    num_locks+=1
  }
  if(currentState.lock3_open){
    num_locks+=1
  }
  switch(num_locks){
    case 0:
      return 'Aus den Lautsprechern hörst du ein hämisches Lachen von INSPECTOR F. ZWÖLF, der sich sichtlich darüber freut, dass noch alle Schlösser zu sind.'
    case 1:
      return '"Soso", hörst du aus den Lautsprechern. "Immerhin ein Schloss offen. Das heisst noch gar nichts! Die anderen beiden sind viiiiel schwerer, BWAHAHAHAHAHA"'
    case 2:
      return 'INSPECTOR F. ZWÖLF scheint langsam etwas nervös zu werden. "Zwei Schlösser..oha.... ähm.. gar nicht mal so schlecht. Aber am letzten wirst du definitiv scheitern, ich schwör!"'
    case 3:
      return "In der Ferne hörst du INSPECTOR F. ZWÖLF laut schwitzen."
    default:
      return ''
  }

}

const textNodes = [
  {
    id: 1,
    text: 'Eigentlich wolltest du hier ja nur kurz ein Giveaway abzügeln und dich dann schleunigst zum nächsten Stand aufmachen, der mit seiner AI-Crypto-Powered Blockchain Serverless Web3-NFT Visualisation vielversprechende Zukunftsaussichten bietet. Aber statt dir einfach einen Lanyard in die Hand zu drücken und dich möglichst ohne Interaktionen ziehen zu lassen, wollen die von dir ernsthaft, dass du irgendein komisches Spiel spielst. OMG. Wie jetzt, und dann auch noch so ein scheussliches Textadventure? Sind die in den 80ern stehen geblieben? Du überlegst dir, ob es die 5 Minuten ernsthaft wert sind und du den START-Button wirklich drücken sollst…',
    options: [
      {
        text: 'START',
        nextText: 3
      },
      {
        text: 'Bloss nicht - schnell weg von hier!',
        nextText: 2
      }
    ]
  },
  {
    id: 2,
    text: 'Eine weise Entscheidung! So einfach lässt du dich von diesem Preis nicht ködern! Du steckst die Hände in die Hosentasche und schlenderst pfeifend davon, als ob nichts gewesen wäre.',
    options: [
    ]
  },
  {
    id: 3,
    text: 'Im Moment als du den Knopf drückst, wird dir plötzlich schwindelig. Die Umgebung ist verschwommen und der Umgebungslärm verschwindet in weiter Ferne. Du reibst dir die Augen. Nanu. Wo sind denn alle hin? Die Messehalle scheint plötzlich menschenleer. Bist du wirklich ganz allein? Du rufst …',
    options: [
      {
        text: 'Hallo?',
        nextText: 4
      },
      {
        text: 'HAAAAALOOOO?!',
        nextText: 4
      },
      {
        text: 'HALLLOO-HOO..',
        nextText: 4
      },
      {
      text: 'nichts.',
      nextText: 4
     }
    ]
  },
  {
    id: 4,
    text: 'Nichts passiert. Nach ein paar Sekunden hörst du ein Knistern von Lautsprechern und eine metallische Stimme ertönt. "CHRZZZZT..BZZZZT… MUAHAHAHAHAHA Du bist mir in die Falle gegangen. Ich bin INSPECTOR F. ZWÖLF und dies ist ist mein METAVERlieS, aus dem du wahrscheinlich nie wieder herauskommen wirst. Dafür müsstest du schon die drei Code-Schlösser an der Tür knacken, aber das hat noch NIEMAND geschafft!',
    options: [
      {
        text: 'Türe anschauen',
        nextText: 6
      },
      {
        text: 'INSIGHT CHECK',
        nextText: 5
      },
    ]
  },
  {
    id: 5,
    text: 'Als erfahrerene:r Dungeons & Dragon Spieler:in kramst du natürlich sofort deinen D20 hervor und würfelst eine 18. Dir wird sofort klar, dass dich INSPECTOR F. ZWÖLF anlügt, natürlich haben andere diesen Code schon geknackt. Das gibt dir Hoffnung. Was die können, kannst du schon lange!',
    options: [
      {
        text: 'Türe anschauen',
        nextText: 6
      }
    ]
  },
  { // Look at the door
    id: 6,
    text: (currentState) => 'An der Türe befinden sich drei Schlösser. Schloss 1 ist: '+boolToDoorLockState(currentState.lock1_open)+'. Schloss 2 ist: '+boolToDoorLockState(currentState.lock2_open)+'. Schloss 3 ist: '+boolToDoorLockState(currentState.lock3_open)+'.\n\n'+inspectorComments(currentState),
    options: [
      {
        text: 'Schloss 1 anschauen.',
        requiredState: (currentState) => !currentState.lock1_open,
        nextText: 7
      },
      {
        text: 'Schloss 2 anschauen.',
        requiredState: (currentState) => !currentState.lock2_open,
        nextText: 8
      },
      {
        text: 'Schloss 3 anschauen.',
        requiredState: (currentState) => !currentState.lock3_open,
        nextText: 9
      },
      {
        text: 'Türe öffnen!',
        requiredState: (currentState) => currentState.lock1_open && currentState.lock2_open && currentState.lock3_open,
        nextText: 10
      },
    ]
  },
  {
    id: 7, // Lock 1
    text: 'Auf dem ersten Schloss siehst du mehrere Knöpfe, angeschrieben mit "OF", "PIECE", "HACK", "CAKE" , "HACKER" , "THE" und "TRACK".  Aus den Lautsprechern hörst du INSPECTOR F. ZWÖLF: "Du hast keine Chance, dieses Schloss zu öffnen. Dazu müsstest du wissen, wie der Escape Room vom SWITCH-CERT Security Awareness Team heisst.. MUAHAHAHAHAHA". Du schaust dir die Knöpfe an und drückst.... ',
    options: [
      {
        text: 'HACK - THE - HACKER',
        setState: { lock1_open: true },
        nextText: 6
      },
      {
        text: 'TRACK - THE - HACKER',
        nextText: 6
      },
      {
        text: 'PIECE - OF - CAKE',
        nextText: 6
      },
      {
        text: 'PIECE - THE - TRACKER - OF - CAKE',
        nextText: 6
      }
    ]
  },
  {
    id: 8, // Lock 2
    text: 'Dieses Schloss zeigt eine SWITCH EDU-ID. Daneben ist ein Post-it aufgeklebt, auf welchem steht "Wie lange bin ich gültig?" Darunter siehst du einen Schieberegeler, auf welchem du einen blau-gelben Hebel an verschiedene angeschriebene Positionen bewegen kannst. Du wählst...',
    options: [
      {
        text: '1 Semester',
        nextText: 6
      },
      {
        text: '1 Jahr',
        nextText: 6
      },
      {
        text: 'Bis zum Studienabschluss',
        nextText: 6
      },
      {
        text: 'Das ganze Leben',
        setState: { lock2_open: true },
        nextText: 6
      }
    ]
  },
  {
    id: 9, // Lock 3
    text: 'Das dritte Schloss verlangt einen 7 stelligen Zahlencode. Als du auf den darunterliegenden "Passwort vergessen?"-Knopf drückst, erscheint kurz eine Meldung: "Anzahl .ch Domains am 15. Februar 2022". Sehr hilfreich! Du gibst folgenden Zahlencode ein: ',
    options: [
      {
        text: '5281993',
        nextText: 6
      },
      {
        text: '1234567',
        nextText: 6
      },
      {
        text: '2478696',
        setState: { lock3_open: true },
        nextText: 6
      },
      {
        text: '7381544',
        nextText: 6
      }
    ]
  },
  {
    id: 10, // 
    text: '"WAAAAS?! DU HAST MICH BESIEGT?!" schreit INSPECTOR F. ZWÖLF. Zähneknirschend lässt er dich gehen und schreit dir noch das Codewort hinterher, mit dem du am Wettbewerb teilnehmen kannst:\n'+atob(SECRET_CODE)+' \n\nStolz füllst du den Wettbewerb-Talon aus und hoffst auf den Hauptpreis. Viel Glück!',
    options: [
    ]
  },

]

startGame()
